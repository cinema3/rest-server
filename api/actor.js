module.exports = (db) => {

  const router = require('express').Router();
  const validator = require('validator');

  router.get('/', async (req, res) => {
    const data = await db.any("SELECT * from cinema.actor ORDER BY movie_count desc")
    res.status(200).json(data);
  });

  /**
   * @swagger
   *
   * /actor/{id}:
   *   get:
   *     tags:
   *     - Acteur
   *     description: Détail de l'auteur
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: id
   *         description: Username to use for login.
   *         in: path
   *         required: true
   *         type: string
   *         format: uuid
   *     responses:
   *       200:
   *         description: acteur
   */
  router.get('/:id', async (req, res) => {
    if (!validator.isUUID(req.params.id, "4")) {
      res.status(404).json({ status: 'error', message: 'bad format uuid', data: { id: req.params.id } });
    }

    try {
      const actor = await db.oneOrNone("SELECT * from cinema.actor where id = $1", req.params.id);

      if (actor == null) {
        res.status(404).json({ status: 'error', message: 'not found', data: { id: req.params.id } });
      }

      actor.roles = await db.manyOrNone("SELECT m.*, c.role, c.alias from cinema.movie as m inner join cinema.castandcrew as c on (c.movie = m.id) where c.person = $1", req.params.id)
      res.status(200).json({ status: 'success', data: actor });
    }
    catch (error) {
      res.status(500).send(error);
    }
  });

  router.get('/:id/movie', async (req, res) => {
    try {
      const data = await db.manyOrNone(`SELECT distinct m.id, m.title, m.year, m.release, m.runtime,
      (SELECT person.firstname || ' ' || person.lastname  FROM cinema.castandcrew
        inner join cinema.person on (castandcrew.person = cinema.person.id)
        WHERE role = 'director' AND castandcrew.movie = m.id LIMIT 1) as director
      from cinema.movie as m
      inner join cinema.castandcrew as c on (c.movie = m.id)
      where c.person = $1 and role = 'actor' order by m.year`, req.params.id)
      res.status(200).json(data);
    }
    catch (error) {
      res.status(200).send(error);
    }
  });

  router.put('/', (req, res) => {

    db.one("insert into cinema.person (firstname, lastname, dob) VALUES ($1, $2, $3) RETURNING id",
      [req.body.firstname, req.body.lastname, req.body.dob])
      .then(function (data) {
        res.status(201).json({ status: 'success', data: data });
      })
      .catch(function (reason) {
        res.status(500).json(reason);
      });
  });

  router.put('/:id', (req, res) => {

    db.one("update cinema.person set firstname = $2, lastname = $3, dob = $4 WHERE id = $1 RETURNING *",
      [req.params.id, req.body.firstname, req.body.lastname, req.body.dob])
      .then(function (data) {
        res.status(201).json({ status: 'success', data: data });
      })
      .catch(function (reason) {
        res.status(500).json(reason);
      });
  });

  router.delete('/:id', (req, res) => {
    db.one("delete from person where id = $1", req.params.id)
      .then(function (data) {
        res.status(200).json({ status: 'success', data: data });
      })
      .catch(function (reason) {
        res.status(500).json(reason);
      })
  });
  return router;
}
