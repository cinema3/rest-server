"use strict";

require("dotenv").config();
const express = require("express");
const app = express();
const cors = require("cors");
const port = process.env.PORT || 3200;
const YAML = require("yamljs");

const { db } = require("./db");
const bodyParser = require("body-parser");

const main = require("./main"),
  actor = require("./api/actor"),
  production = require('./api/production');

var pubkey = `-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE9fYxdCsDLmYkMYJvkCMRvsBb5Tqr
ZxCwPRB5Oiz51EDeBSWT8wnanMM/F1oGXGIjXIePzZHBMwOiYlsu34ItzQ==
-----END PUBLIC KEY-----`;


/* app.use(function (req, res, next) {
  var token = req.headers.authorization;
  var decoded = jwt.verify(token, pubkey);
  console.log(decoded)
  if (decoded && decode.roles.includes('manager'))
  {
   next();
  }
  else
  {
    res.send(401);
  }
});
*/

app.use(cors({ origin: 'null' }));
app.use(bodyParser.json());

app.use('/', main());
app.use('/api/actor', actor(db));
app.use('/api/production', production(db));

/*
const swaggerUI = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');;
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));
*/

app.listen(port, () => {
  console.log(`Server is running on ${port} port.`);
})
