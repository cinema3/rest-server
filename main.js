module.exports = () => {

  const router = require('express').Router();

  router.get('/', (req, res) => {
    res.status(200).json("Application Cinéma");
  });

  router.get('/version', async (req, res) => {
    res.status(200).send("1.0");
  });

  return router;
}
